
const redis = require("async-redis");

class ProcessConnections {

  #thisStage = 'dev'
  #redisHost = 'dev-mqtt-redis-aws-001.onz7hw.0001.apn2.cache.amazonaws.com'
  #currentClient = null

  client = () => {

    return new Promise(async (resolve, reject) => {
      try {
        let client = await redis.createClient({host: this.#redisHost, port: 6379, db: 9})
        resolve(client)
      } catch(e) {
        console.log(e)
        reject(e)
      }
    })
  }

  get = (key) => {

    return new Promise(async (resolve, reject) => {
      try {
        if (this.#currentClient == null) {
          this.#currentClient = await this.client()
        }
        let value = await this.#currentClient.get(key)
        resolve(value)
      } catch(e) {
        reject(e)
      }
    })
  }

  set = (key, value) => {

    return new Promise(async (resolve, reject) => {
      try {
        if (this.#currentClient == null) {
          this.#currentClient = await this.client()
        }
        await this.#currentClient.set(key, value)
        resolve(true)
      } catch(e) {
        reject(e)
      }
    })
  }

  del = (keys) => {

    return new Promise(async (resolve, reject) => {
      try {
        if (this.#currentClient == null) {
          this.#currentClient = await this.client()
        }
        await this.#currentClient.del(keys)
        resolve(true)
      } catch(e) {
        reject(e)
      }
    })
  }

  mset = (keyValues) => {

    return new Promise(async (resolve, reject) => {
      try {
        if (this.#currentClient == null) {
          this.#currentClient = await this.client()
        }
        await this.#currentClient.mset(keyValues)
        resolve(true)
      } catch(e) {
        reject(e)
      }
    })
  }

  hmset = (key, hashKey, value) => {

    return new Promise(async (resolve, reject) => {
      try {
        if (this.#currentClient == null) {
          this.#currentClient = await this.client()
        }
        await this.#currentClient.hmset(key, [hashKey, value])
        resolve(true)
      } catch(e) {
        reject(e)
      }
    })
  }

  hkeys = (key) => {

    return new Promise(async (resolve, reject) => {
      try {
        if (this.#currentClient == null) {
          this.#currentClient = await this.client()
        }
        let values = await this.#currentClient.hkeys(key)
        resolve(values)
      } catch(e) {
        reject(e)
      }
    })
  }

  hdel = (key, hashKey) => {

    return new Promise(async (resolve, reject) => {
      try {
        if (this.#currentClient == null) {
          this.#currentClient = await this.client()
        }
        await this.#currentClient.hdel(key, hashKey)
        resolve(true)
      } catch(e) {
        reject(e)
      }
    })
  }

  applySubscriptions = async (payload) => {

    let clientId = payload.clientId

    if (!clientId.startsWith('ap-northeast-2')) {
      // console.log('is not app')
      return
    }

    let topics = payload.topics
    let firstTopic = topics[0]
    let firstTopicComponents = firstTopic.split('/')

    let stage = firstTopicComponents[0]
    let type = firstTopicComponents[1]

    if (firstTopicComponents.length == 3 && stage == this.#thisStage && type == 'user') {
      // console.log('is user topic')

      let userId = firstTopicComponents[2]

      let userKey = `mqtt-client-id-user:${userId}`
      let clientIdKey = `mqtt-client-id:${clientId}`

      try {
        if (payload.eventType == 'subscribed') {
          console.log('subscribed', userId, clientId)
          await this.mset([userKey, clientId, clientIdKey, userId])
        } else {
          console.log('unsubscribed', userId, clientId)
          await this.del([userKey, clientIdKey])
        } 
      } catch (e) {
        console.error(e)
      }

    } else if (firstTopicComponents.length == 4 && stage == this.#thisStage && type == 'message' && (firstTopicComponents[3] == 'All' || firstTopicComponents[3] == '#')) {

      let roomId = firstTopicComponents[2]
      let roomInKey = `mqtt-room-in:${roomId}`

      let clientIdKey = `mqtt-client-id:${clientId}`
      let userId = await this.get(clientIdKey)
      let userRoomInKey = `mqtt-room-in-user:${userId}`

      if (payload.eventType == 'subscribed') {
        console.log('room subscribed', roomId, userId, clientId)
        await this.hmset(roomInKey, userId, clientId)
        await this.set(userRoomInKey, roomId)
      } else {
        console.log('room unsubscribed', roomId, userId, clientId)
        await this.hdel(roomInKey, userId)
        await this.del(userRoomInKey)
      }

    } else {
      // console.log('is not valid topic')
    }
  }

  applyDisconnected = async (payload) => {
    // console.log(payload)

    let clientId = payload.clientId

    if (!clientId.startsWith('ap-northeast-2')) {
      // console.log('is not app')
      return
    }

    let clientIdKey = `mqtt-client-id:${clientId}`
    let userId = await this.get(clientIdKey)
    let userKey = `mqtt-client-id-user:${userId}`

    let userRoomInKey = `mqtt-room-in-user:${userId}`
    let roomId = await this.get(userRoomInKey)
    let roomInKey = `mqtt-room-in:${roomId}`
    
    try {
      console.log('disconnected', userId, clientId)
      await this.del([userKey, clientIdKey, userRoomInKey])
      if (roomId != null) {
        await this.hdel(roomInKey, userId)
      }
    } catch (e) {
      console.error(e)
    }
  }

  connectedUserIds = async (roomId, userId) => {
    let roomInKey = `mqtt-room-in:${roomId}`
    let userIds = await this.hkeys(roomInKey)
    userIds = userIds.map(userId => {
      return parseInt(userId)
    })
    if (!userIds.includes(userId)) {
      userIds.push(userId)
    }
    return userIds
  }
}

module.exports = new ProcessConnections()