
'use strict';

const awsIot = require('aws-iot-device-sdk')
const fs = require('fs')

const keyPath = `certs/OhingBackend.private.key` 
const certPath = `certs/OhingBackend.cert.pem` 
const caPath = `certs/AmazonRootCA1.pem` 
const region = 'ap-northeast-2'
const port = 8883
const endpoint = 'a1tlskjn4p1j8f-ats.iot.ap-northeast-2.amazonaws.com'

const dev = require('./dev.js')
const applyConnections = require('./applyConnections.js')

const thisStage = 'dev'

module.exports.prepare = async () => {

  let clientId = `ohing-broker`

  let device = awsIot.device({
      keyPath: keyPath,
      certPath: certPath,
      caPath: caPath,
      clientId: clientId,
      region: region,
      port: port,
      host: endpoint,
      debug: true
  })

  device.on('connect', () => {
      console.log('connected')

      device.subscribe(`${thisStage}/message`, {qos: 1}, (error, topic) => {
        console.log('subscribed', error, topic)
      })
      device.subscribe('$aws/events/subscriptions/subscribed/#', {qos: 1}, (error, topic) => {
        console.log('subscribed', error, topic)
      })
      device.subscribe('$aws/events/subscriptions/unsubscribed/#', {qos: 1}, (error, topic) => {
        console.log('subscribed', error, topic)
      })
      device.subscribe('$aws/events/presence/disconnected/#', {qos: 1}, (error, topic) => {
        console.log('subscribed', error, topic)
      })
  })

  device.on('message', async (topic, payload) => {
    let string = payload.toString()
    console.log(topic, string)

    let components = topic.split('/')

    if (components[0] == '$aws') {
      console.log('aws topic')

      if (components[2] == 'subscriptions') {
        let object = JSON.parse(string)
        applyConnections.applySubscriptions(object)
        return
      } else if (components[2] == 'presence' && components[3] == 'disconnected') {
        let object = JSON.parse(string)
        applyConnections.applyDisconnected(object)
        return
      }
    }

    if (components.length != 2) {
      console.log('invalid topic')
      return
    }

    let stage = components[0]

    let object = JSON.parse(string)

    if (stage == thisStage) {
      let [completed, topics] = await dev.completed(object, applyConnections)
      if (completed != null) {
        console.log(completed, topics)

        for (let topic of topics) {
          device.publish(topic, JSON.stringify(completed), {qos: 1}, (error, data) => {
            console.log('published', error, data)
          })
        }
        await dev.sendPush(completed.chatMessage)
      }
    } else {
      console.log('invalid stage')
    }
  })

  device.on('error', (error) => {
      console.error(error)
  })

  device.on('close', () => {
      console.error('closed')
  })

  device.on('offline', () => {
      console.error('offline')
      device = null
  })

  device.on('reconnect', () => {
      console.error('reconnect')
  })
}

module.exports.prepare()