
const mysql2 = require('mysql2/promise')
const camelcase = require('camelcase-keys')
const axios = require('axios')

const AWS = require('aws-sdk')
AWS.config.update({region: 'ap-northeast-2'})
const lambda = new AWS.Lambda()

class Dev {

  #thisStage = 'dev'

  currentClient = null

  client = async () => {
    try {
        return await mysql2.createConnection({
          host: 'ohing-migration-cluster.cluster-cztt5idwty9x.ap-northeast-2.rds.amazonaws.com', 
          user: 'ohing_master',
          password: 'ohing123',
          database: 'ohingmig'
        })
    } catch(e) {
        console.log(e)
        throw e
    }
  }
  
  escape = async (component) => {
    try {

      if (this.currentClient == null) {
        console.log('begin connect')
        this.currentClient = await this.client()
      }

      return await this.currentClient.escape(component)

    } catch (e) {

      console.log(e)

        for (let retryCount = 0; retryCount < 2; retryCount++) {

          console.log('reconnecting...')
          this.currentClient = await this.client()

          try {
            let [result, _] = await this.currentClient.execute(query)
            return result
          } catch (e) {
            console.log(e)
          }
        }

        throw(e)
    }
  }

  execute = async (query, parameters) => {

    try {

      if (this.currentClient == null) {
        console.log('begin connect')
        this.currentClient = await this.client()
      }
      let [result, _] = await this.currentClient.execute(query, parameters)

      return result

    } catch(e) {
        console.log(e)

        for (let retryCount = 0; retryCount < 2; retryCount++) {

          console.log('reconnecting...')
          this.currentClient = await this.client()

          try {
            let [result, _] = await this.currentClient.execute(query)
            return result
          } catch (e) {
            console.log(e)
          }
        }

        throw(e)
    }
  }

  completed = async (payload, applyConnections) => {

    let messageId = payload.messageId

    if (messageId != null) {

      query = `
        select 
          cm.msg_id message_id, cm.msg_type_cd type, cm.room_id, cm.send_user_id user_id, cm.send_user_nick_nm nickname,
          cm.send_profile_image_url profile_image_url, substring_index(cm.send_profile_image_url, '/', -1) profile_file_name,
          cm.chat_profile_intro_content chat_profile_self_introduction, cm.master_yn is_master,
          cm.send_dt_int timestamp, cm.msg_content text, cm.media_info,
          cm.target_user_id, cm.target_user_nick_nm target_user_nickname,
          cm.invite_room_info,
          count(cmr.receive_user_id) unread_count  
        from
          oi_chat_msg cm
          left join oi_user_info ui on cm.send_user_id = ui.user_id
          left join oi_chat_msg_receive cmr on cm.msg_id = cmr.msg_id and cmr.receive_yn = 'N'
        where
          cm.msg_id = ${messageId};
      `
    }

    let roomId = payload.roomId
    let timestamp = payload.timestamp
    let userId = payload.userId

    if (roomId == null || timestamp == null || userId == null) {
      console.log('invalid payload')
      return [null, null]
    }

    let query
    let result
    let valueParameters

    query = `
      select
        ui.user_status_cd, ui.profile_open_type_cd, cui.use_yn
      from
        oi_chat_user_info cui
        join oi_user_info ui on cui.user_id = ui.user_id
      where
        cui.user_id = ${userId}
        and cui.room_id = ${roomId};
    `
    result = await this.execute(query)

    let userInfo = result[0]
    // console.log(query, result)
    if (userInfo == null || userInfo.user_status_cd != 'ACTIVE' || userInfo.profile_open_type_cd == 'CLOSE' || userInfo.use_yn != 'Y') {
      console.log('unavailable user')
      return [null, null]
    }

    query = `
      select 
        room_type_cd, room_status_cd
      from
        oi_chat_room_info
      where
        room_id = ${roomId};
    `
    result = await this.execute(query)
    let roomType = result[0].room_type_cd
    let roomStatus = result[0].room_status_cd

    if (roomStatus != 'ACTIVE') {
      console.log('unavailable room')
      return [null, null]
    }

    query = `
      select 
        cm.msg_id message_id, cm.msg_type_cd type, cm.room_id, cm.send_user_id user_id, cm.send_user_nick_nm nickname, 
        cm.send_profile_image_url profile_image_url, substring_index(cm.send_profile_image_url, '/', -1) profile_file_name,
        cm.chat_profile_intro_content chat_profile_self_introduction, cm.master_yn is_master,
        cm.send_dt_int timestamp, cm.msg_content text, cm.media_info,
        cm.target_user_id, cm.target_user_nick_nm target_user_nickname,
        cm.invite_room_info,
        count(cmr.receive_user_id) unread_count  
      from
        oi_chat_msg cm
        left join oi_user_info ui on cm.send_user_id = ui.user_id
        left join oi_chat_msg_receive cmr on cm.msg_id = cmr.msg_id and cmr.receive_yn = 'N'
      where
        cm.room_id = ${roomId}
        and cm.send_user_id = ${userId}
        and cm.send_dt_int = ${timestamp}
      limit 
        1;
    `

    result = await this.execute(query)

    if (result.length > 0 && result[0].message_id != null) {

      let message = result[0]
      message.is_master = message.is_master == 'Y'
      message = camelcase(message, {deep: true})
      console.log('exists', message)
      
      let target = message.targetUserId != null ? `${message.targetUserId}` : 'All'

      return [{
        type: 'ChatMessage',
        chatMessage: message
      }, [`${this.#thisStage}/message/${message.roomId}/${target}`]]
    }

    if (roomType == 'ONE') {
      query = `
        update
          oi_chat_user_info
        set
          use_yn = 'Y',
          join_dt = ${timestamp-1}
        where
          room_id = ${roomId}
          and use_yn = 'N';
      `
      await this.execute(query)
    }

    let textValue = payload.text || null

    let mediaInfoValue = payload.mediaInfo

    if (mediaInfoValue != null && !Array.isArray(mediaInfoValue)) {
      return [null, null]
    }

    if ((mediaInfoValue || []).length == 0) {
      mediaInfoValue = null
    }

    let encodingMovies = []
    let encoded = 'Y'

    if (mediaInfoValue != null) {

      let i = 1

      for (let mediaInfo of mediaInfoValue) {

        if (mediaInfo.mediaKey == null || mediaInfo.type == null) {
          console.log('invalid mediaInfo', mediaInfo)
          return [null, null]
        }

        if (mediaInfo.type == 'MOVIE') {
            encoded = 'N'
            encodingMovies.push({
              key: mediaInfo.mediaKey,
              seq: i
            })
          }

        if (mediaInfo.width == null) {
          mediaInfo.width = 600
        }
        if (mediaInfo.height == null) {
          mediaInfo.height = 600
        }

        i += 1
      }
    }

    let mediaInfo = mediaInfoValue == null ? null : JSON.stringify(mediaInfoValue)

    let targetUserId = payload.targetUserId || null
    let inviteRoomId = payload.inviteRoomId || null

    query = `
      select 
        cpi.chat_profile_id,
        case when cpi.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname,
        substring_index(case when cpi.chat_profile_id is null then fi.org_file_nm else fi2.org_file_nm end, '/', -1) profile_file_name
      from
        oi_chat_user_info cui
        left join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi on upi.file_id = fi.file_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
        left join oi_file_info fi2 on cpi.image_id = fi2.file_id
      where
        cui.room_id = ${roomId}
        and cui.user_id = ${userId};
    `
    // console.log(query)
    result = await this.execute(query)

    if (result.length == 0) {
      console.log('invalid room id')
      return [null, null]
    }

    let profileFileNameValue = result[0].profile_file_name
    let profileFileName = (profileFileNameValue || '').length == 0 ? null : profileFileNameValue
    let nicknameValue = result[0].nickname || null

    let chatProfileId = result[0].chat_profile_id || null
    
    let targetUserNicknameValue = null
    let targetUserNickname = null

    if (targetUserId == null) {
      targetUserId = null
      targetUserNickname = null
    } else {
      let query = `
        select 
          case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname
        from
          oi_chat_user_info cui
          left join oi_user_info ui on cui.user_id = ui.user_id
          left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
        where
          cui.room_id = ${roomId}
          and cui.user_id = ${targetUserId}
          and cui.use_yn = 'Y';
      `
      result = await this.execute(query)
      if (result.length == 0) {
        console.log('invalid targetUserId')
        return [null, null]
      }
      targetUserNicknameValue = result[0].nickname 
      targetUserNickname = targetUserNicknameValue
    }

    query = `
      select 
        cri.room_id, cri.title, cri.join_user_cnt join_user_count, cri.max_user_cnt max_user_count,
        substring_index(fi.org_file_nm, '/', -1) cover_file_name,
        case when cui.chat_profile_id is null then substring_index(fi2.org_file_nm, '/', -1) else substring_index(fi3.org_file_nm, '/', -1) end profile_file_name,
        case when cui.chat_profile_id is null then ui.user_login_id else cpi.nick_nm end nickname,
        case 
          when length(cri.room_pwd) > 0 then 1
          else 0
        end needs_password,
        concat(
          '[',
          (
            select
              group_concat(concat('"', ti.tag_val, '"') separator ',')
            from 
              oi_chat_room_info cri2
              join oi_chat_room_tag crt on cri2.room_id = crt.room_id
              join oi_tag_info ti on crt.tag_id = ti.tag_id
            where 
              cri2.room_id = cri.room_id
            order by
              crt.tag_seq asc
          ),
          ']'
        ) tags
        
      from
        oi_chat_room_info cri
        left join oi_file_info fi on cri.room_image_id = fi.file_id
        join oi_chat_user_info cui on cri.room_id = cui.room_id and cui.use_yn = 'Y' and cui.master_yn = 'Y'
        join oi_user_info ui on cui.user_id = ui.user_id
        left join oi_user_profile_image upi on ui.user_id = upi.user_id and upi.image_type_cd = 'PF'
        left join oi_file_info fi2 on upi.file_id = fi2.file_id
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
        left join oi_file_info fi3 on cpi.image_id = fi3.file_id

      where
        cri.room_id = ${inviteRoomId}
    `

    result = await this.execute(query)

    // console.log(result[0])
    let inviteRoomInfo = null
    if (result.length > 0) {
      let roomInfo = result[0]
      roomInfo.tags = JSON.parse(roomInfo.tags)
      roomInfo.needs_password = roomInfo.needs_password == 1
      let camelcased = camelcase(roomInfo, {deep: true})
      inviteRoomInfo = camelcased
    }

    query = `
      select 
        case
          when cpi.chat_profile_id is null then null
          else ifnull(cpi.self_intro_content, '')
        end self_introduction,
        cui.master_yn
      from
        oi_chat_user_info cui
        left join oi_chat_profile_info cpi on cui.chat_profile_id = cpi.chat_profile_id
      where
        cui.room_id = ${roomId}
        and cui.user_id = ${userId};
    `
    // console.log(query)

    result = await this.execute(query)

    let selfIntroductionValue = result[0].self_introduction || null
    let masterYn = result[0].master_yn

    let messageType = (inviteRoomId == null || inviteRoomId == 'null') ? 'CHAT' : 'OPENINVITE'

    let inviteRoomInfoValue = inviteRoomInfo == null ? null : JSON.stringify(inviteRoomInfo)

    let chatHash = `${roomId}-${userId}-${timestamp}`

    query = `
      insert into oi_chat_msg (
        room_id, msg_type_cd, msg_content, send_dt_int, send_user_id, send_user_nick_nm, send_profile_image_url, target_user_id, target_user_nick_nm, chat_profile_intro_content, master_yn,
        invite_room_id, invite_room_info, media_info, encoded_yn, chat_hash
      ) values (
        ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
        ?, ?, ?, ?, ?
      );
    `
    // console.log(query)
    try {
      valueParameters = [
        roomId, messageType, textValue, timestamp, userId, nicknameValue, profileFileName, targetUserId, targetUserNickname, selfIntroductionValue, masterYn,
        inviteRoomId, inviteRoomInfoValue, mediaInfo, encoded, chatHash
      ]
      console.log('values', valueParameters)
      result = await this.execute(query, valueParameters)
      messageId = result.insertId
    } catch (e) {
      console.log(e)
      console.log('exists and do not send')
      return [null, null]
    }
    // console.log(result)

    if (encodingMovies.length > 0) {

      let values = []

      for (let movie of encodingMovies) {
        values.push(`
          ( ${messageId}, '${movie.key}' )
        `)
      }

      query = `
        insert into oi_chat_media_encoding_info (
          msg_id, media_key
        ) values
          ${values.join(',')};
      `
      await this.execute(query)
    }

    let exceptUserIds = await applyConnections.connectedUserIds(roomId, userId)

    query = `
      insert into oi_chat_msg_receive (
        room_id, msg_id, receive_user_id
      )  
      select 
        cm.room_id, cm.msg_id, cui.user_id
      from 
        oi_chat_msg cm 
        join oi_chat_user_info cui on cm.room_id = cui.room_id
      where
        cm.msg_id = ${messageId}
        and cui.use_yn = 'Y'
        and cui.user_id not in(${exceptUserIds.join(',')});
    `
    result = await this.execute(query)

    // console.log('aff', result)

    let message = {
	    messageId: messageId,
	    type: 'CHAT',
	    roomId: roomId,
	    userId: userId, // 발송자 아이디
	    chatProfileId: chatProfileId, // Optional. 있으면 채팅프로필입니다.
      chatProfileSelfIntroduction: selfIntroductionValue, // Optional. 있으면 채팅프로필입니다.
      isMaster: masterYn == 'Y',
	    nickname: nicknameValue, 
	    profileFileName: profileFileNameValue, // Optional
	    timestamp: timestamp, // milisecond unix timestamp, 
	    text: textValue, // Optional
	    mediaInfo: mediaInfoValue, // Optional
	    targetUserId: targetUserId, // Optional.
	    targetUserNickname: targetUserNicknameValue, // Optional.
	    inviteRoomInfo: inviteRoomInfo,
      unreadCount: 1,
	  }

    let completed = {
      type: 'ChatMessage',
      chatMessage: message
    }

    // console.log('completed', completed)

    let target = targetUserId != null ? targetUserId : 'All'
    let topics = [`${this.#thisStage}/message/${message.roomId}/${target}`]

    if (targetUserId != null) {
      topics.push(`${this.#thisStage}/message/${message.roomId}/${userId}`)
    }

    // console.log('topics', topics)

    // let userIds

    // if (targetUserId != 'null') {
    //   userIds = [userId, targetUserId]
    // } else {
    //   query = `
    //     select
    //       user_id
    //     from 
    //       oi_chat_user_info
    //     where
    //       room_id = ${message.roomId}
    //       and use_yn = 'Y';
    //   `
    //   result = await this.execute(query)

    //   userIds = result.map(row => {
    //     return row.user_id
    //   })
    // }

    return [completed, topics]
  }

  androidOptions = {
        priority: 'high'
  }

  iosOptions = {
      headers: {
          'apns-priority': 10,
      }
  }

  fcmTopicPayload = (topic, notification, data = null) => {
    notification['content-available'] = true,
    notification['mutable-content'] = true
    return {
      to: `/topics/${topic}`,
      data: data,
      notification: notification,
      android: this.androidOptions,
      apns: this.iosOptions
    }
  }

  fcmToPayload = (to, notification, data = null) => {
    notification['content-available'] = true,
    notification['mutable-content'] = true
    return {
      to: to,
      data: data,
      notification: notification,
      android: this.androidOptions,
      apns: this.iosOptions
    }
  }

  fcmIdsPayload = (ids, notification, data = null) => {

    let payload = {
      registration_ids: ids,
      data: data,
      android: this.androidOptions,
      apns: this.iosOptions
    }

    if (notification != null) {
      notification['content-available'] = true,
      notification['mutable-content'] = true

      payload.notification = notification
    }

    return payload
  }

  axiosOptions = {
    headers: {
      'Authorization': `key=AAAAl4zZDWQ:APA91bFM-ImE3Er_pov80-XmmeVnX9oXe6YS4WjUsd7npagM6LTMqL7Z6uRCud3kGsBcQVuVxQtLMV9R4tLAlm9D511gCtwHTQkeYN878_zVkqYKzG7so4XkXCbIKAQUa_Nd9zw4AUE_`
    }
  }

  sendPush = async (message) => {

    // console.log('AAA', message)

    const sendMessageEndpoint = 'https://fcm.googleapis.com/fcm/send'

    let roomId = message.roomId

    let query = `
      select  
        room_type_cd type, title
      from 
        oi_chat_room_info
      where
        room_id = ${roomId}
    `
    let result = await this.execute(query)
    let roomType = result[0].type

    let title = result[0].title || '오잉챗'
    let body = ''

    switch (message.type) {
      case 'CHGMASTER':
      case 'DEPORT':
      case 'GROUPINVITE':
      case 'ROOMINMSG':
      case 'ROOMOUT':
      case 'BLOWUP':
        body = message.text
        break
      case 'OPENINVITE':
        body = '오픈채팅방에 초대합니다.'
        break
      default:
        if (message.mediaInfo != null) {
          body = `${message.nickname}: ${message.targetUserId == null ? '' : '(귓속말)'} (미디어)`
        } else {
          body = `${message.nickname}: ${message.targetUserId == null ? '' : '(귓속말)'}${message.text}`
        }
        break
    }

    if (roomType == 'OTHER') {

      let fcmTopic = `openchat-${roomId}`

      try {

        let notification = {
          body: body
        } 

        let url = sendMessageEndpoint
        let params = this.fcmTopicPayload(fcmTopic, notification, message)
        // console.log('will push', sendMessageEndpoint, params)

        let result = await axios.post(url, params, this.axiosOptions)
      } catch (e) {
        console.log(e)
        console.error(Object.keys(e), (e.response || {}).status, (e.response || {}).data)
      }

    } else {

      let userId = message.userId

      let userIdCondition = message.targetUserId != null ? `
        and cui.user_id in (${message.targetUserId}, ${message.userId})
      ` : ''

      query = `
        select 
          udi.push_auth_key, os_type_cd, cui.alarm_yn
        from 
          oi_chat_user_info cui
          join oi_user_device_info udi on cui.user_id = udi.user_id and udi.login_yn = 'Y' and push_auth_key is not null and length(udi.push_auth_key) > 0
          join oi_user_info ui on udi.user_id = ui.user_id and ui.user_status_cd = 'ACTIVE' and ui.profile_open_type_cd != 'CLOSE'
        where 
          cui.room_id = ${roomId}
          ${userIdCondition}
          and cui.use_yn = 'Y';
      `
      result = await this.execute(query)

      let iosTokens = []
      let androidTokens = []

      for (let row of result) {
        if (row.os_type_cd == 'ANDROID') {
          androidTokens.push(row.push_auth_key)
        } else if (row.os_type_cd == 'IOS' && row.alarm_yn == 'Y') {
          iosTokens.push(row.push_auth_key)
        }
      }

      if (iosTokens.length > 0 || androidTokens.length > 0) {

        let notification = {
          title: title,
          body: body
        } 

        try {

          let url = sendMessageEndpoint

          // console.log('FCM', '------------')
          // console.log('FCM', url)

          let data = {
            type: 'ChatMessage',
            chatMessage: message
          }

          if (iosTokens.length > 0) {
            let iosParams = this.fcmIdsPayload(iosTokens, notification, data)
            // console.log('FCM', iosParams)

            let iosResult = await axios.post(url, iosParams, this.axiosOptions)
            // console.log('FCM', iosResult.data)
          }

          if (androidTokens.length > 0) {

            data.chatMessage.title = notification.title
            data.chatMessage.body = notification.body
            let androidParams = this.fcmIdsPayload(androidTokens, null, data)
            // console.log('FCM', androidParams)

            let androidResult = await axios.post(url, androidParams, this.axiosOptions)
            // console.log('FCM', androidResult.data)
          }

          // console.log('FCM', '------------')

        } catch (e) {
          console.error(e)
          // console.error(Object.keys(e), (e.response || {}).status, (e.response || {}).data)
        }
      }
    }

    let params = {
      FunctionName: `api-new-batch-${this.#thisStage}-sendUpdatedRoomInfo`,
      InvocationType: "Event",
      Payload: JSON.stringify({
        body: JSON.stringify({
          roomId: message.roomId,
        })
      })
    }

    await lambda.invoke(params).promise() 
  }
}

module.exports = new Dev()